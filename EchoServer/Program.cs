﻿// Initially taken from https://www.geeksforgeeks.org/socket-programming-in-c-sharp/

using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace EchoServer
{
    class Program
    {
        private static bool thread_clients = true;
        private static bool thread_listener = false;
        private static Int16 max_clients = 3;
        private static Thread listeningThread = null;
        private static ConcurrentBag<Thread> clientThreads = new ConcurrentBag<Thread>();


        // Main Method 
        static void Main(string[] args)
        {
            // Handle Ctrl-c
            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e) {
                Console.WriteLine("Captured Ctrl-C");
                TerminateServer();
                return;
            };
            if (thread_listener)
            {
                Console.WriteLine("Starting threaded listener");
                listeningThread = new Thread(() => ListenHandler());
                listeningThread.Name = "Listening thread";
                listeningThread.Start();
                while (true)
                {
                    string command = Console.ReadLine();
                    if (command == "quit")
                    {
                        TerminateServer();
                        break;
                    }
                }
            }
            else
            {
                // Do we want to thread the listening stream?
                // The listen itself is still blocking so how does logging / other background tasks work if the main thread is blocked?
                Console.WriteLine("Starting non-threaded listener");
                ListenHandler();
                Console.WriteLine("Listener returned.");
            } 

            Console.WriteLine("Done");
        }

        static void TerminateServer()
        {
            // Terminate the listening thread
            // Terminate the client threads
        }

        private static void ListenHandler(Int16 listenPort = 7777)
        {
            // Listen to listenPort 
            IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
            Int16 bind_address_index = -1;
            Int16 clientCount = 0;

            for (Int16 i = 0; i < ipHost.AddressList.Length; i++)
            {
                // This whole chunk should be unneccesary but I have VMware installed and I have 22 network adapters.

                // Filter IPv6
                if(ipHost.AddressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                {
                    continue;
                }
                // Filter ips not 192.168.1.*
                string ip_prefix = ipHost.AddressList[i].ToString().Substring(0, 10);
                if(string.Compare(ip_prefix, "192.168.1.") != 0)
                {
                    continue;
                }
                bind_address_index = i;
            }

            if(bind_address_index == -1)
            {
                Console.WriteLine("No appropriate addresses found to bind to.");
                return;
            }
            Console.WriteLine("Binding to: {0} on port {1}", ipHost.AddressList[bind_address_index], listenPort);
            IPAddress ipAddr = ipHost.AddressList[bind_address_index];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddr, listenPort);

            try
            {
                // Creation TCP/IP Socket using  
                // Socket Class Costructor 
                Socket listener = new Socket(ipAddr.AddressFamily,
                                SocketType.Stream, ProtocolType.Tcp);

                listener.Bind(localEndPoint);
                listener.Listen(10);
                
                while (true)
                {
                    Console.WriteLine("Waiting for connection...");
                    Socket clientSocket = listener.Accept();

                    if (thread_clients == true)
                    {
                        // Fork a new thread for each client.
                        Console.WriteLine("We have client #{0}, forking handler thread: {1}!!!", clientCount, 
                            clientSocket.RemoteEndPoint.ToString());
                        Thread t = new Thread(() => ClientHandler(clientSocket));
                        t.Name = $"client-{clientSocket.RemoteEndPoint.ToString()}";
                        t.Start();
                        // Save th thread handle so we can reference later for killing.
                        clientThreads.Add(t);
                    }
                    else
                    {                        
                        ClientHandler(clientSocket);
                    }
                    clientCount += 1;
                    if (clientCount >= max_clients)
                    {
                        Console.WriteLine("Hit client limit terminating listening thread");
                        break;
                    }
                }

                // Stop listening
                listener.Close();

                Thread.Sleep(30000);

                Console.WriteLine("Terminating clients");
                foreach (Thread currThread in clientThreads)
                {
                    if (currThread.IsAlive == true)
                    {
                        Console.WriteLine("Terminating Thread '{0}'", currThread.Name);
                        currThread.Interrupt();
                        currThread.Join();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        private static void ClientHandler(Socket clientSocket)
        {
            try
            {
                Console.WriteLine("Entered client handler thread for client: {0}", clientSocket.RemoteEndPoint.ToString());

                byte[] welcomeMessage = System.Text.Encoding.ASCII.GetBytes("Welcome to this echo server\r\n");

                // Data buffer 
                byte[] recvBuffer = new Byte[1024];
                string curr_chunk = null;
                string data = null;

                clientSocket.Send(welcomeMessage);

                while (true)
                {
                    Boolean terminateClient = false;
                    int numByte = clientSocket.Receive(recvBuffer);

                    if (numByte == 0)
                        terminateClient = true;

                    Console.WriteLine("Recv binary len {0}: {1}", numByte, BitConverter.ToString(recvBuffer, 0, numByte));

                    curr_chunk = Encoding.ASCII.GetString(recvBuffer, 0, numByte);

                    Console.WriteLine("Chunk recv from client: '{0}'", curr_chunk);

                    data += curr_chunk;

                    Int32 endOfLineIndex = data.IndexOf("\r\n");
                    if (endOfLineIndex > -1)
                    {
                        string reply_data = data.Substring(0, endOfLineIndex);
                        string remainder_data = data.Substring(endOfLineIndex + 2);

                        if (reply_data == "quit") // Case-senstive
                        {
                            Console.WriteLine("Found quit message, terminating client");
                            reply_data = "quit acknowledged, goodbye...";
                            terminateClient = true;
                        }
                        else
                        {
                            Console.WriteLine("Found end of line, echoing to user: {0}", reply_data);
                        }

                        byte[] reply = Encoding.ASCII.GetBytes(reply_data + "\r\n");
                        clientSocket.Send(reply);

                        data = remainder_data;
                    }

                    // Has the user requested to terminate or have we reached the EOF marker?
                    if (terminateClient)
                        break;
                }
            }
            catch (ThreadInterruptedException)
            {
                Console.Write("Thread forcibly terminated!");
                byte[] reply = Encoding.ASCII.GetBytes("Server shutting down\r\n");
                clientSocket.Send(reply);
            }
            finally
            {
                Console.WriteLine("Closing connection to: {0}", clientSocket.RemoteEndPoint.ToString());
                clientSocket.Shutdown(SocketShutdown.Both);
                clientSocket.Close();
            }
        }
    }
}
